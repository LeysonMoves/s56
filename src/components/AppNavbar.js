import {Navbar, Nav} from 'react-bootstrap';
import { Link, NavLink} from 'react-router-dom';
import logo from "../items/logo3.png";


export default function AppNavbar(){

	return(
		<Navbar className= "sticky-top" bg="dark" variant="dark" expand="lg">
		  <Navbar.Brand  as ={NavLink} to="/products" exact><div className="brand" fw-bold >LM STREET LAB</div></Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
		      <Nav.Link as ={NavLink} to="/home" exact>Home</Nav.Link>
		      <Nav.Link as ={NavLink} to="/products" exact>Products</Nav.Link>
		      <Nav.Link as ={NavLink} to="/register" exact>Register</Nav.Link>
		      <Nav.Link as ={NavLink} to="/login" exact>Login</Nav.Link>
		      <Nav.Link as ={NavLink} to="/admin" exact>Admin</Nav.Link>	      
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>

  )
}