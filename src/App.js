import { Container } from 'react-bootstrap';
import { useState, useEffect} from 'react';

import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Admin from './pages/Admin';
import { UserProvider } from './UserContext';
import './App.css';



function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  },[user])
 
  return (
      <UserProvider value={{user, setUser, unsetUser}}>

      <Router>
        <AppNavbar/>
        <Container>
          <Switch>
            <Route exact path="/home" component={Home} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/admin" component={Admin} />
          </Switch>
        </Container>
      </Router>
      </UserProvider>



  );
}

export default App;
