
import { Fragment } from 'react';
import { Card,Button,Col, Row, Container } from 'react-bootstrap';
import { Carousel} from 'react-bootstrap';
import image1 from "../items/slide1.png";
import image2 from "../items/slide2.png";
import image3 from "../items/slide3.png";
import image4 from "../items/four.png";
import image5 from "../items/five.png";
import image6 from "../items/six.png";
import image7 from "../items/seven.png";
import image8 from "../items/eight.png";
import image9 from "../items/nine.png";




export default function Products(){
 

  return(
    <Fragment>
    <Container fluid>
    <div className= "carousel">
      <h1 className ="title">Products</h1>
      <Carousel xs={12} md={4}>
        <Carousel.Item interval={2000}>
          <img
            className="d-block w-100"
            src={image1} height ="600" 
            alt="First slide"
          />
        </Carousel.Item>
       <Carousel.Item interval={2000}>
          <img
            className="d-block w-100"
            src={image2} height ="600" 
            alt="Second slide"
          />

        </Carousel.Item>
        <Carousel.Item interval={2000}>
          <img
            className="d-block w-100"
            src={image3} height ="600" 
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
      
    </div>


      <h1 className ="allProducts" >All Products</h1>
   <Row > 
   
     <Col className = "items" xs={12} md={4}>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={image4} />
        <Card.Body>
          <Card.Title>Price: Php100,000</Card.Title>
          <Card.Text>
            Grab a pack of coffee beans to match the Jordan 1 Retro High Travis Scott. 
            This AJ1 comes with a brown upper plus white accents, black Nike "Swoosh", 
            sail midsole, and a brown sole.
          </Card.Text>
          <Button className="addToCartButton"  variant="warning">Add To Cart</Button>
        </Card.Body>
      </Card>
      </Col>

     <Col className = "items" xs={12} md={4}>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={image5} />
        <Card.Body>
          <Card.Title>Price: Php100,000</Card.Title>
          <Card.Text>
            Grab a pack of coffee beans to match the Jordan 1 Retro High Travis Scott. 
            This AJ1 comes with a brown upper plus white accents, black Nike "Swoosh", 
            sail midsole, and a brown sole.
          </Card.Text>
          <Button className="addToCartButton" variant="warning" >Add To Cart</Button>
        </Card.Body>
      </Card>
     </Col>

     <Col className = "items" xs={12} md={4}>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={image6} />
        <Card.Body>
          <Card.Title>Price: Php100,000</Card.Title>
          <Card.Text>
            Grab a pack of coffee beans to match the Jordan 1 Retro High Travis Scott. 
            This AJ1 comes with a brown upper plus white accents, black Nike "Swoosh", 
            sail midsole, and a brown sole.
          </Card.Text>
          <Button className="addToCartButton"  variant="warning">Add To Cart</Button>
        </Card.Body>
      </Card>
      </Col>


     <Col className = "items" xs={12} md={4}>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={image7} />
        <Card.Body>
          <Card.Title>Price: Php100,000</Card.Title>
          <Card.Text>
            Grab a pack of coffee beans to match the Jordan 1 Retro High Travis Scott. 
            This AJ1 comes with a brown upper plus white accents, black Nike "Swoosh", 
            sail midsole, and a brown sole.
          </Card.Text>
          <Button className="addToCartButton" variant="warning" >Add To Cart</Button>
        </Card.Body>
      </Card>
     </Col>

     <Col className = "items" xs={12} md={4}>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={image8} />
        <Card.Body>
          <Card.Title>Price: Php100,000</Card.Title>
          <Card.Text>
            Grab a pack of coffee beans to match the Jordan 1 Retro High Travis Scott. 
            This AJ1 comes with a brown upper plus white accents, black Nike "Swoosh", 
            sail midsole, and a brown sole.
          </Card.Text>
          <Button className="addToCartButton" variant="warning" >Add To Cart</Button>
        </Card.Body>
      </Card>
     </Col>

     <Col className = "items" xs={12} md={4}>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={image9} />
        <Card.Body>
          <Card.Title>Price: Php100,000</Card.Title>
          <Card.Text>
            Grab a pack of coffee beans to match the Jordan 1 Retro High Travis Scott. 
            This AJ1 comes with a brown upper plus white accents, black Nike "Swoosh", 
            sail midsole, and a brown sole.
          </Card.Text>
          <Button className="addToCartButton" variant="warning">Add To Cart</Button>
        </Card.Body>
      </Card>
     </Col>

     
    </Row>
    


    </Container>
    </Fragment>
  )
}



