
import { Fragment } from 'react';
import { Card,Button,Col, Row, Form, Container } from 'react-bootstrap';


export default function Login(){
	
	return(

		<Fragment>
		<Container fluid>
		<h1 className = "title">Login</h1>
		
		<Form >
		    <Row >
		    <Col xs={12} md={6}  className ="login">
		      <Form.Control placeholder="Email" />
		    </Col>
		    <Col xs={12} md={6}  className ="login">
		      <Form.Control placeholder="Password" />
		    </Col>
		    </Row>
		    <Row >
		    <Col className="d-flex justify-content-center">
		     <Button className="loginButton" variant ="dark">Sign In</Button>
		     </Col>
		  </Row>
		</Form>
		</Container>

		</Fragment>
	)
}