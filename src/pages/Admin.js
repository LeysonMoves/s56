import React from 'react';
import { Fragment } from 'react';
import { Card,Button,Col, Row, Form, Container, ButtonGroup , DropdownButton,Dropdown, Table } from 'react-bootstrap';
import image9 from "../items/nine.png";

 

export default function Admin(){
		
		return(
			<Fragment>
				
				<h1 className = "title">Admin</h1>
			<Container>
			<h2 className= "dashboardTitle">Dashboard</h2>
			</Container>
			<Container	fluid className	="pl-3">
				<Row>
				  <Col sm={3}>
						<ButtonGroup vertical className ="dashboard">
						  <Button className	="dashboardButton" variant ="dark">Products</Button>
						  <Button className	="dashboardButton" variant ="dark">Inbox</Button>
						  <Button className	="dashboardButton" variant ="dark">Orders</Button>
						  <Button className	="dashboardButton" variant ="dark">Users</Button>
						  <Button className	="dashboardButton" variant ="dark">Logout</Button>			 
						</ButtonGroup>
				  </Col>
				  
				  <Col sm={8}>
				  <Button className	="editButton" variant ="danger" >Add</Button>
				  <Button className	="editButton" variant ="danger" >Delete</Button>
				  </Col>
				</Row>
				<Row>
				<Table striped bordered hover variant="dark">
				  <thead>
				    <tr>
				      <th>Item No.</th>
				      <th>First Name</th>
				      <th>Last Name</th>
				      <th>Email</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>1</td>
				      <td>Mark</td>
				      <td>Otto</td>
				      <td>@mdo</td>
				    </tr>
				    <tr>
				      <td>2</td>
				      <td>Jacob</td>
				      <td>Thornton</td>
				      <td>@fat</td>
				    </tr>
				    <tr>
				      <td>3</td>
				      <td>Larry the Bird</td>
				      <td>@twitter</td>
				      <td>@fat</td>
				    
				    </tr>
				  </tbody>
				</Table>
				</Row>
			</Container>

			</Fragment>

	)
}
/*

				  <Card  className = "dashboardCard" border="dark" style={{ width: '15rem' }}>    
				    <Card.Body className="dashboard">
				      <Card.Title>Dashboard	</Card.Title>	      
				    </Card.Body>
				  </Card>
				  
				  <Card className = "dashboardCard" border="dark" style={{ width: '15rem' } }>				    
				    <Card.Body className="dashboard">
				      <Card.Title>Orders</Card.Title>				      
				    </Card.Body>
				  </Card>

				  <Card className = "dashboardCard" border="dark" style={{ width: '15rem' }}>
				    <Card.Body className="dashboard">
				      <Card.Title>Inbox</Card.Title>
				    </Card.Body>
				  </Card>
				 
				  <Card className = "dashboardCard"  border="dark" style={{ width: '15rem' }}>				    
				    <Card.Body className="dashboard">
				      <Card.Title>Reports</Card.Title>				   
				   </Card.Body>
				  </Card>
				  
				  <Card className = "dashboardCard"  border="dark" style={{ width: '15rem' }}>				  
				    <Card.Body className="dashboard">
				      <Card.Title>Settings</Card.Title>			     
				    </Card.Body>
				  </Card>
*/